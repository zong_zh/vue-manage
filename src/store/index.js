import Vue from 'vue'
import Vuex from 'vuex'

import tab from './tab'
import user from './user'

Vue.use(Vuex)

// 创建一个新的 store 实例
export default new Vuex.Store({
  modules: {
	  tab,user
  }
})
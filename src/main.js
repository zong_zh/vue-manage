import Vue from 'vue'
import App from './App.vue'

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import Axios from 'axios'

import './api/mock.js'

// 公共样式
import './assets/less/index.less';

// router 才是Vue实例化的配置字段名称，写个其他的它当然不认识了。真是低级错误。
import router from './router/index.js';

import store from './store'


// 全局注入ElementUI
Vue.use(ElementUI);
Vue.config.productionTip = false;

Vue.prototype.$http = Axios;

console.log(router)
new Vue({
    /*created() {
        // 解决添加动态路由后 刷新页面白屏
        store.commit('addMenu', router);
    },*/
    store,
    router,
    render: h => h(App)
}).$mount('#app');
